importScripts('./common.js');

self.addEventListener('message', function(event) {
    if (event.data === 'start') {
        const spentTime = getSpentTime();

        event.source.postMessage(spentTime)
    }
});

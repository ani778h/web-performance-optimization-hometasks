const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin'); // Add this line

module.exports = {
    entry: path.resolve(__dirname, './index.js'),
    output: {
        filename: "[name].mjs"
    },
    mode: 'development',
    module: {
        rules: [
            {
                test: /\.html$/i,
                loader: "html-loader",
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            ['@babel/preset-env', { modules: false }]
                        ]
                    }
                }
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./index.html",
        }),
        new ScriptExtHtmlWebpackPlugin({
            module: /\.modern\.js$/, // Add module attribute to .modern.js files
            minify: {
                safari10: true,
                removeAttributeQuotes: false
            }
        })
    ],
};

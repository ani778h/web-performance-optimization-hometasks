const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');

module.exports = {
    entry: path.resolve(__dirname, './index.js'),
    output: {
        filename: "[name].bundle.js"
    },
    mode: 'development',
    module: {
        rules: [
            {
                test: /\.html$/i,
                loader: "html-loader",
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            ['@babel/preset-env', { targets: "ie 11" }]
                        ]
                    }
                }
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({ template: "./index.html" }),
        new ScriptExtHtmlWebpackPlugin({
            default: 'defer',
            custom: [
                {
                    test: /\.js$/,
                    attribute: "nomodule",
                    value: true
                }
            ]
        })
    ],
};
